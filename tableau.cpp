// Librarie pour pouvoir utiliser ifstream/ofstream
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
// Lire un fichier ligne par ligne
// Entr�e : le chemin d'acc�s au fichier
void readFile(const std::string& path)
{
    std::vector<std::string> tab;

    std::ifstream ifs(path.c_str());
    if (ifs) // test si le fichier est bien ouvert
    {
        std::string line;
        while (std::getline(ifs, line)) // lecture ligne par ligne
        {
             tab = line ;

        }
        ifs.close(); // fermeture du flux
    }
    else // en cas d'erreur...
    {
        std::cout << "Cannot read " << path << std::endl;
    }
}
